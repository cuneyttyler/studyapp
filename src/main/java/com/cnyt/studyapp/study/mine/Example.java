package com.cnyt.studyapp.study.mine;

import java.util.stream.IntStream;

public class Example {
    private int x = 6;
    private int y = 5;
    private Example example;
    public int[] array = new int[] {};

    public Example() {
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int func() {
        return 0;
    }

    public static void main(String[] args) {
        Example example = new Example();
        System.out.println(example.getX());
        example.setX(5);
        System.out.println(example.getX());

        if(example.func() == 5) {
            System.out.println("evet");
        } else if (example.getX() == 0){
            System.out.println("hayir");
        } else {
            System.out.println("hicbiri");
        }

        int arr[] = IntStream.rangeClosed(1, 5).toArray();
        if(arr[2] == 3) {
            System.out.println("evet 3");
        }

        for(int i = 0; i < arr.length; i++) {
            arr[i] = 0;
        }

        int i = 0;
        while(i < arr.length) {
            arr[i++] = 0;
        }


    }
}