package com.cnyt.studyapp.study.mine;

import java.util.Scanner;

public class Mastermind {
    private char[] tutulan;
    private char[][] board;
    private int n, limit;

    public Mastermind(int n, int limit) {
        this.n = n;
        this.limit = limit;
        this.board = new char[limit][n];
        this.tutulan = new char[n];
    }

    public static void main(String[] args) throws Exception{
        Mastermind mastermind = new Mastermind(5,10);

        System.out.println("Give your input : ");
        for(int i = 0; i < mastermind.n; i++) {
            // Using Scanner for Getting Input from User
            Scanner in = new Scanner(System.in);
            String line = in.nextLine();

            while(line.length() > 1 || (!line.equals("G") && !line.equals("Y") && !line.equals("R") && !line.equals("O") && !line.equals("B"))) {
                System.out.println("Wrong input : Only G, Y, R, O or B");
                System.out.println("Give your input : ");
                line = in.nextLine();
            }
            mastermind.tutulan[i] = line.charAt(0);
        }

        int i =0;
        char[] guess = new char[mastermind.n];
        char[] guessResult = new char[mastermind.n];
        char[] temp = new char[mastermind.n];
        int trueGuessCount = 0;
        while(i < mastermind.limit && trueGuessCount < mastermind.n) {
            for(int j = 0; j < mastermind.n; j++) {
                System.out.println("Give your guess : ");
                Scanner in = new Scanner(System.in);
                String line = in.nextLine();

                while(line.length() > 1 || (!line.equals("G") && !line.equals("Y") && !line.equals("R") && !line.equals("O") && !line.equals("B"))) {
                    System.out.println("Wrong input : Only G, Y, R, O or B");
                    System.out.println("Give your guess : ");
                    line = in.nextLine();
                }
                guess[j] = line.charAt(0);
            }

            for(int j = 0; j < mastermind.n; j++) {
                temp[j] = mastermind.tutulan[j];
                guessResult[j] = '-';
            }

            for(int j = 0; j < mastermind.n; j++) {
                if(guess[j] == mastermind.tutulan[j]) {
                    guessResult[j] = 'B';
                    temp[j] = '-';
                } else {
                    for(int k = 0; k < mastermind.n; k++) {
                        if(guess[j] == temp[k]) {
                            guessResult[j] = 'W';
                            temp[k] = '-';
                            break;
                        }
                    }
                }
            }

            for(int j = 0; j < mastermind.n; j++) {
                mastermind.board[i][j] = guessResult[j];
            }
            for(int j = 0; j < i+1; j++) {
                for(int k = 0; k < mastermind.n; k++) {
                    System.out.print(mastermind.board[j][k] + " ");
                }
                System.out.println();
            }

            trueGuessCount = 0;
            for(int j = 0; j < mastermind.n; j++) {
                if(guessResult[j] == 'B') {
                    trueGuessCount++;
                }
            }
            i++;
        }

        if(trueGuessCount == mastermind.n) {
            System.out.println("You win!");
        } else {
            System.out.println("You lose!");
        }
    }


}
