package com.cnyt.studyapp.study.array;

public class FindPond {

    static int[] r = {0, -1, 1, -1, 1, 0, 1, 1, 0, 1, -1, 1, -1, 0, -1, -1};

    static boolean isValid(int[][] m, int x, int y) {
        return y >= 0 && x >= 0 && y < m.length && x < m[0].length;
    }

    static int max = 0;

    static int BFS(int[][] m, int x, int y, int waterLevel, boolean[][] visited) {

        visited[y][x] = true;

        int count = 0;
        if(m[y][x] <= waterLevel)
            count++;
        else
            return 0;

        for(int i = 0; i < r.length; i+=2) {
            int xx = x + r[i];
            int yy = y + r[i+1];
            if(isValid(m, xx,yy ) && !visited[yy][xx] && m[yy][xx] <= waterLevel) {
                count += BFS(m,xx,yy,waterLevel,visited);
            }
        }

        max = Math.max(max,count);

        return count;
    }

    static int findPond(int[][] m, int x, int y, int waterLevel, boolean[][] visited) {
        return BFS(m,x,y,waterLevel, visited);
    }

    static int findLargestPond(int[][] m, int waterLevel, boolean[][] visited) {
        for(int i = 0; i < m[0].length; i++) {
            for(int j = 0; j < m.length; j++) {
                BFS(m,i,j,waterLevel,visited);
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[][] m = new int[][]
                {
                        {1, 2, 3},
                        {5, 5, 6},
                        {4, 4, 3},
                        {5, 4, 4}
                };

        boolean[][] visited = new boolean[4][3];

        System.out.println(findPond(m,1,0,4, visited));
        System.out.println(findLargestPond(m,4, visited));
    }
}
