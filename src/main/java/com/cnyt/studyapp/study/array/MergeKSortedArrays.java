package com.cnyt.studyapp.study.array;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class MergeKSortedArrays {
    static class Node {
        int val;
        int arrNo;
        int index;

        Node(int v, int a, int i) {
            val = v;
            arrNo = a;
            index = i;
        }
    }
    //Function to merge k sorted arrays.
    public static ArrayList<Integer> mergeKArrays(int[][] arr,int K)
    {
        ArrayList<Integer> result = new ArrayList<>();

        PriorityQueue<Node> queue = new PriorityQueue<>((n1, n2)-> n1.val-n2.val);
        int[] indexes = new int[K];
        for(int i=0; i<K;i++) {
            queue.add(new Node(arr[i][0],i,0));
            indexes[i]++;
        }

        while(!queue.isEmpty()) {
            Node node = queue.poll();

            result.add(node.val);

            if(node.index < arr[node.arrNo].length) {
                queue.add(new Node(arr[node.arrNo][node.index],node.arrNo,node.index+1));
            }
        }

        return result;
    }
}
