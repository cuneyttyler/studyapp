package com.cnyt.studyapp.study.array;

import java.util.Arrays;

public class LargestThreeProduct {

    static int[] find(int[] arr) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int[] maxIndexes = new int[3], minIndexes = new int[3];
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length; j++) {
                if(i != j) {
                    int mult = arr[i] * arr[j];
                    if(max < mult) {
                        maxIndexes[0] = i;
                        maxIndexes[1] = j;
                        max = mult;
                    }
                    if(min > mult) {
                        minIndexes[0] = i;
                        minIndexes[1] = j;
                        min = mult;
                    }
                }
            }
        }

        int max2 = Integer.MIN_VALUE, max3 = Integer.MIN_VALUE;
        for(int i = 0; i < arr.length; i++) {
            if(i != maxIndexes[0] && i != maxIndexes[1] && max2 < arr[i] * max) {
                max2 = arr[i] * max;
                maxIndexes[2] = i;
            }
            if(i != minIndexes[0] && i != minIndexes[1] && max3 < arr[i] * min) {
                max3 = arr[i] * min;
                minIndexes[2] = i;
            }
        }

        if(max2 > max3)
            return maxIndexes;
        else
            return minIndexes;
    }

    public static void main(String[] args) {
        int[] result = find(new int[]{5,3,-10,4,-5});
        System.out.println(Arrays.toString(result));
    }
}
