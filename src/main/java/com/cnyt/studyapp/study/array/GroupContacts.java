package com.cnyt.studyapp.study.array;

import java.util.*;

public class GroupContacts {
    static List<List<Contact>> group(List<Contact> contacts) {
        Map<String,List<Contact>> phoneMap = new HashMap<>();
        Map<String,List<Contact>> nameMap = new HashMap<>();

        for(Contact c : contacts) {
            phoneMap.computeIfAbsent(c.phone, k -> new ArrayList<>()).add(c);
            nameMap.computeIfAbsent(c.name, k -> new ArrayList<>()).add(c);
        }

        List<List<Contact>> result = new ArrayList<>();
        for(List<Contact> cList : nameMap.values())
            result.add(cList);

        for(List<Contact> cList : phoneMap.values())
            result.add(cList);

        return result;
    }

    // abc -> "abc","9987" - "abc","9985" - "abc","9986"
    // xyz -> "xyz","9986"
    // abd -> "abd","9986"
    // 9987 -> "abc","9987"
    // 9986 -> "xyz","9986" - "abd","9986" - "abc","9986"
    // 9985 -> "abc","9985"
    public static void main(String[] args) {
        List<Contact> list = Arrays.asList(new Contact("abc","9987"),
                new Contact("xyz","9986"),
                new Contact("abc","9985"),
                new Contact("abd","9986"),
                new Contact("abc","9986"));

        List<List<Contact>> groups = group(list);

        for(List<Contact> group : groups) {
            System.out.println(group);
        }
    }

    static class Contact {
        String name;
        String phone;

        Contact(String n, String p) {
            name = n;
            phone = p;
        }

        @Override
        public String toString() {
            return name + ", " + phone;
        }
    }
}
