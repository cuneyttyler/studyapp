package com.cnyt.studyapp.study.array;

public class FindExcelColumnName {
    // ASCII A : 65
    static int ASCII = 64;
    static int NUMBER_OF_LETTERS = 26;
    static void find(int columnNumber) {
        StringBuilder columnName = new StringBuilder();

        while (columnNumber > 0) {
            // Find remainder
            int rem = columnNumber % 26;

            // If remainder is 0, then a
            // 'Z' must be there in output
            if (rem == 0) {
                columnName.append("Z");
                columnNumber = (columnNumber / 26) - 1;
            }
            else // If remainder is non-zero
            {
                columnName.append((char)((rem - 1) + 'A'));
                columnNumber = columnNumber / 26;
            }
        }

        // Reverse the string and print result
        System.out.println(columnName.reverse());
    }

    // 1..26            A..Z
    // 26+1..26+26*26        AA..ZZ
    // 26*26+1..26*26+1+1+26*26*26 AAA.ZZZ
    // 703 AAA
    // 705 AAC
    // 729 AAZ
    // 730 ABA
    // (705)%26 B
    //

    public static void main(String[] args) {
        find(26);
    }
}
