package com.cnyt.studyapp.study.array;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class StringsVersionOrdering {

    static Comparator<Data> comparator = new Comparator<Data>() {
        @Override
        public int compare(Data d1, Data d2) {
            if(d1.isVersionNumeric() && !d2.isVersionNumeric())
                return 1;
            else if (!d1.isVersionNumeric() && d2.isVersionNumeric())
                return -1;
            else if(d1.version.equals(d2.version)) {
                return d1.id.compareTo(d2.id);
            } else {
                return d1.version.compareTo(d2.version);
            }
        }
    };

    static String[] sort(String[] arr) {
        Queue<Data> queue = new PriorityQueue<>(comparator);

        for(String e : arr) {
            String[] strs = e.split(" ");
            queue.add(new Data(strs[0].toCharArray()[0],strs[1].toCharArray()[0]));
        }

        String[] result = new String[arr.length];
        int i = 0;
        while(!queue.isEmpty()) {
            Data d = queue.poll();
            result[i++] = d.id + " " + d.version;
        }

        return result;
    }

    public static void main(String[] args) {
        String[] result = sort(new String[]{"a a","c b","b b","a 2","b 1"});
        System.out.println(Arrays.toString(result));
    }

    // a a, b b, c b, a 1, b 2
    static class Data{
        Character id;
        Character version;

        Data(Character id, Character version){
            this.id = id;
            this.version = version;
        }

        Boolean isVersionNumeric() {
            try {
                Integer.parseInt(version.toString());
                return true;
            } catch(NumberFormatException e) {
                return false;
            }
        }
    }
}
