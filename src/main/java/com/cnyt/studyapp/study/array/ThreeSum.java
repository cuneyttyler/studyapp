package com.cnyt.studyapp.study.array;

import java.util.*;

public class ThreeSum {

    static Map<Integer,Integer> map = new HashMap<>();
    public static int[] twoSum(int[] nums, int target, int j) {
        for(int i = 0; i < nums.length; i++) {
            if(map.containsKey(target - nums[i])
                    && map.get(target - nums[i]) != j
                    && j != i)
                return new int[]{nums[i],target - nums[i]};
        }
        return new int[]{};
    }

    public static List<List<Integer>> threeSum(int[] nums) {
        for(int i = 0; i < nums.length; i++) map.put(nums[i],i);

        List<List<Integer>> result = new ArrayList<>();
        for(int i = 0; i < nums.length; i++) {
            int[] complement = twoSum(nums,-nums[i],i);
            if(complement.length > 0) {
                List<Integer> currentResult = Arrays.asList(nums[i],complement[0],complement[1]);
                if(result.stream().allMatch(l -> !l.containsAll(currentResult)))  result.add(currentResult);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        List<List<Integer>> result = threeSum(new int[] {2,7,11,15,-22});
        for(List<Integer> list : result) System.out.println(list);
    }
}
