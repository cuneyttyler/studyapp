package com.cnyt.studyapp.study.array;

public class FindAppeaeringOnce {
    static int findOnce(int arr[], int n)
    {
        return bs(arr,n,0,n-1);
    }

    static int bs(int[] arr,int n, int l, int r) {
        if(l>r) return -1;

        int mid = (l + r) / 2;

        if(mid == 0 && arr[mid+1] != arr[mid]) return mid;

        if(mid == n-1 && arr[mid-1] != arr[mid]) return mid;

        if(arr[mid-1] == arr[mid] && (mid-l + 1) % 2 == 0)
            return bs(arr,n,mid+1,r);

        if(arr[mid-1] == arr[mid] && (mid-l + 1) % 2 == 1)
            return bs(arr,n,l,mid-1);

        if(arr[mid+1] == arr[mid] && (r-mid+1) % 2 == 0)
            return bs(arr,n,l,mid-1);

        if(arr[mid+1] == arr[mid] && (r-mid+1) % 2 == 1)
            return bs(arr,n,mid+1,r);

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(findOnce(new int[]{1, 1, 2, 2, 3, 3, 4, 50, 50, 65, 65},11));
    }
}
