package com.cnyt.studyapp.study.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// A class to store a graph edge
class Edge
{
    int source, dest, weight;

    public Edge(int source, int dest, int weight)
    {
        this.source = source;
        this.dest = dest;
        this.weight = weight;
    }
}

// A class to represent a graph object
class Graph
{
    // A list of lists to represent an adjacency list
    List<List<Edge>> adjList = null;

    // Constructor
    Graph(List<Edge> edges, int N)
    {
        adjList = new ArrayList<>();

        for (int i = 0; i < N; i++) {
            adjList.add(new ArrayList<>());
        }

        // add edges to the undirected graph
        for (Edge edge: edges) {
            adjList.get(edge.source).add(edge);
        }
    }
}

class ShortestPathBellmanFord
{
    // Perform DFS on the graph and set the departure time of all
    // vertices of the graph
    private static int DFS(Graph graph, int v, boolean[] discovered,
                           int[] departure, int time)
    {
        // mark the current node as discovered
        discovered[v] = true;

        // set arrival time – not needed
        // time++;

        // do for every edge `v —> u`
        for (Edge edge: graph.adjList.get(v))
        {
            int u = edge.dest;

            // if `u` is not yet discovered
            if (!discovered[u]) {
                time = DFS(graph, u, discovered, departure, time);
            }
        }

        // ready to backtrack
        // set departure time of vertex `v`
        departure[time] = v;
        time++;

        return time;
    }

    // The function performs the topological sort on a given DAG and then finds
    // the longest distance of all vertices from a given source by running one pass
    // of the Bellman–Ford algorithm on edges or vertices in topological order
    public static void findShortestDistance(Graph graph, int source, int N)
    {
        // `departure[]` stores the vertex number using departure time as an index
        int[] departure = new int[N];
        Arrays.fill(departure, -1);

        // to keep track of whether a vertex is discovered or not
        boolean[] discovered = new boolean[N];
        int time = 0;

        // perform DFS on all undiscovered vertices
        for (int i = 0; i < N; i++)
        {
            if (!discovered[i]) {
                time = DFS(graph, i, discovered, departure, time);
            }
        }

        int[] cost = new int[N];
        Arrays.fill(cost, Integer.MAX_VALUE);

        cost[source] = 0;

        // Process the vertices in topological order, i.e., in order
        // of their decreasing departure time in DFS
        for (int i = N - 1; i >= 0; i--)
        {
            // for each vertex in topological order,
            // relax the cost of its adjacent vertices
            int v = departure[i];
            for (Edge e: graph.adjList.get(v))
            {
                // edge `e` from `v` to `u` having weight `w`
                int u = e.dest;
                int w = e.weight;

                // if the distance to destination `u` can be shortened by
                // taking edge `v —> u`, update cost to the new lower value
                if (cost[v] != Integer.MAX_VALUE && cost[v] + w < cost[u]) {
                    cost[u] = cost[v] + w;
                }
            }
        }

        // print shortest paths
        for (int i = 0; i < N - 1; i++) {
            System.out.printf("dist(%d, %d) = %2d\n", source, i, cost[i]);
        }
    }

    public static void main(String[] args)
    {
        // List of graph edges as per the above diagram
        List<Edge> edges = Arrays.asList(
                new Edge(0, 6, 2), new Edge(1, 2, -4),
                new Edge(1, 4, 1), new Edge(1, 6, 8),
                new Edge(3, 0, 3), new Edge(3, 4, 5),
                new Edge(5, 1, 2), new Edge(7, 0, 6),
                new Edge(7, 1, -1), new Edge(7, 3, 4),
                new Edge(7, 5, -4)
        );

        // total number of nodes in the graph
        final int N = 8;

        // build a graph from the given edges
        Graph graph = new Graph(edges, N);

        // source vertex
        int source = 7;

        // find the shortest distance of all vertices from the given source
        findShortestDistance(graph, source, N);
    }
}