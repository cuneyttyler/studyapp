package com.cnyt.studyapp.study.graph;

import java.util.*;

public class CourseScheduleMine {
     String WHITE = "WHITE";
     String GRAY = "GRAY";
     String BLACK = "BLACK";

     Map<Integer, List<Integer>> adjMap = new HashMap<>();
     Map<Integer,String> color = new HashMap<>();
     boolean isPossible = true;
     Stack<Integer> resultStack = new Stack<>();

    public  int[] findOrder(int numCourses, int[][] prerequisites) {
        for(int i=0; i<numCourses; i++){
            color.put(i,WHITE);
        }
        for(int[] adj : prerequisites) {
            int src = adj[1];
            int dest = adj[0];
            adjMap.computeIfAbsent(src, k -> new ArrayList<>()).add(dest);
        }
        
        for(int i=0; i<numCourses; i++) {
            if(color.get(i).equals(WHITE)) {
                dfs(i);
            }
        }

        if(isPossible) {
            int[] result = new int[numCourses];
            int i = 0;
            while (!resultStack.isEmpty()) {
                result[i++] = resultStack.pop();
            }

            return result;
        } else {
            return new int[]{};
        }
    }
    
     void dfs(int root) {
        color.put(root, GRAY);

        for(Integer adj : adjMap.getOrDefault(root, new ArrayList<>())) {
            if(color.get(adj).equals(GRAY)) {
                isPossible = false;
                break;
            }
            if(color.get(adj).equals(WHITE)) {
                dfs(adj);
            }
        }

        color.put(root, BLACK);
        resultStack.push(root);
    }

    public  static void main(String[] args) {
        int[] result = new CourseScheduleMine().findOrder(4,new int[][]{{1,0},{2,0},{3,1},{3,2}});
        System.out.println(Arrays.toString(result));
    }
}
