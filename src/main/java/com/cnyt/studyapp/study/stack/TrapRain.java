package com.cnyt.studyapp.study.stack;

import java.util.Stack;

public class TrapRain {
    public int trapBruteForce(int[] height) {
        int ans = 0;
        for(int i=0; i<height.length; i++) {
            int leftMax = 0, rightMax = 0;
            for(int j=i; j>=0; j--) {
                leftMax = Math.max(leftMax,height[j]);
            }
            for(int j=i; j<height.length; j++) {
                rightMax = Math.max(rightMax,height[j]);
            }
            ans += Math.min(leftMax,rightMax) - height[i];
        }

        return ans;
    }

    int trapStack(int[] height)
    {
        int ans = 0, current = 0;
        Stack<Integer> st = new Stack<>();
        while (current < height.length) {
            while (!st.empty() && height[current] > height[st.peek()]) {
                int top = st.peek();
                st.pop();
                if (st.empty())
                    break;
                int distance = current - st.peek() - 1;
                int bounded_height = Math.min(height[current], height[st.peek()]) - height[top];
                ans += distance * bounded_height;
            }
            st.push(current++);
        }
        return ans;
    }

    public static void main(String[] args) {
        int result = new TrapRain().trapStack(new int[]{0,1,0,2,1,0,1,3,2,1,2,1});
        System.out.println(result);
    }
}
