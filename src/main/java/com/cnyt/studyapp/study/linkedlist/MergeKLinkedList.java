package com.cnyt.studyapp.study.linkedlist;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class MergeKLinkedList {

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
    
    public static ListNode mergeKLists(ListNode[] lists) {
        Comparator<ListNode> cmp;
        cmp = new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                // TODO Auto-generated method stub
                return o1.val-o2.val;
            }
        };

        Queue<ListNode> q = new PriorityQueue<ListNode>(cmp);
        for(ListNode l : lists){
            if(l!=null){
                q.add(l);
            }
        }
        ListNode head = new ListNode(0);
        ListNode point = head;
        while(!q.isEmpty()){
            point.next = q.poll();
            point = point.next;
            ListNode next = point.next;
            if(next!=null){
                q.add(next);
            }
        }
        return head.next;
    }
    
    static void print(ListNode head) {
        System.out.print(head.val);
        while(head.next != null) {
            head = head.next;
            System.out.print(" -> " + head.val);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ListNode node11 = new ListNode(11);
        ListNode node10 = new ListNode(10,node11);
        ListNode node9 = new ListNode(9,node10);
        ListNode node8 = new ListNode(8,node9);

        ListNode node7 = new ListNode(7);
        ListNode node5 = new ListNode(5,node7);
        ListNode node3 = new ListNode(3,node5);
        ListNode node1 = new ListNode(1,node3);

        ListNode node6 = new ListNode(6);
        ListNode node4 = new ListNode(4,node6);
        ListNode node2 = new ListNode(2,node4);

        ListNode result = mergeKLists(new ListNode[]{node1,node2,node8});
        print(result);
    }
}
