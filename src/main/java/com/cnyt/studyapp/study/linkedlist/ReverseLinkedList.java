package com.cnyt.studyapp.study.linkedlist;

public class ReverseLinkedList {

    public static class ListNode {
      int val;
      ListNode next;
      ListNode() {}
      ListNode(int val) { this.val = val; }
      ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public static ListNode reverseKGroup(ListNode head, int k) {
        ListNode prev = null;
        ListNode curr = head;
        ListNode next = null;
        int count = 0;

        int len = length(head);
        if(len < k)
            return head;

        // reverse first k nodes;
        while(curr != null && count <k){
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;

            count++;
        }

        // apply recursion for rest of the nodes
        if(next != null)
            head.next = reverseKGroup(next, k);

        return prev;
    }

    private static int length(ListNode head){
        int count = 0;
        while(head != null){
            count++;
            head = head.next;
        }
        return count;
    }

    static void print(ListNode head) {
        System.out.print(head.val);
        while(head.next != null) {
            head = head.next;
            System.out.print(" -> " + head.val);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ListNode node7 = new ListNode(7);
        ListNode node6 = new ListNode(6,node7);
        ListNode node5 = new ListNode(5,node6);
        ListNode node4 = new ListNode(4,node5);
        ListNode node3 = new ListNode(3,node4);
        ListNode node2 = new ListNode(2,node3);
        ListNode node1 = new ListNode(1,node2);

        print(node1);

        ListNode reversed = reverseKGroup(node1,3);
        print(reversed);
    }
}
