package com.cnyt.studyapp.study.linkedlist;

public class MergeSortedLinkedLists {
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    public ListNode mergeTwoListsRecursive(ListNode l1, ListNode l2){
        if(l1 == null) return l2;
        if(l2 == null) return l1;
        if(l1.val < l2.val){
            l1.next = mergeTwoListsRecursive(l1.next, l2);
            return l1;
        } else{
            l2.next = mergeTwoListsRecursive(l1, l2.next);
            return l2;
        }
    }

    public static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1 == null) return l2;
        else if(l2 == null) return l1;
        ListNode dummy = new ListNode(0);
        ListNode curr = dummy;
        while(l1 != null && l2!= null){
            if(l1.val <= l2.val){
                curr.next = l1;
                l1 = l1.next;
            }else {
                curr.next = l2;
                l2 = l2.next;
            }
            curr = curr.next;
        }
        curr.next = l1 == null? l2:l1;
        return dummy.next;
    }

    static void print(ListNode head) {
        System.out.print(head.val);
        while(head.next != null) {
            head = head.next;
            System.out.print(" -> " + head.val);
        }
        System.out.println();
    }
    
    public static void main(String[] args) {
        ListNode node7 = new ListNode(7);
        ListNode node5 = new ListNode(5,node7);
        ListNode node3 = new ListNode(3,node5);
        ListNode node1 = new ListNode(1,node3);

        ListNode node6 = new ListNode(6);
        ListNode node4 = new ListNode(4,node6);
        ListNode node2 = new ListNode(2,node4);

        ListNode result = mergeTwoLists(node1,node2);
        print(result);
    }
}
