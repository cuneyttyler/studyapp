package com.cnyt.studyapp.openpayd.service.external.impl;

import com.cnyt.studyapp.openpayd.exception.SymbolNotFoundException;
import com.cnyt.studyapp.openpayd.service.external.RatesApiService;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Map;

@Component
@Configuration
public class RatesApiServiceImpl implements RatesApiService {

    private String RATES_API_URL = "https://api.ratesapi.io/api/latest";
    @Override
    public Double getExchangeRate(String sourceCurrency, String targetCurrency) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        try {
            ResponseEntity<Map> response = new RestTemplate().exchange(RATES_API_URL + "?base=" + sourceCurrency + "&symbols=" + targetCurrency,
                    HttpMethod.GET, entity, Map.class);

            Map<String, Double> rates = (Map) response.getBody().get("rates");
            return rates.get(targetCurrency);
        } catch (HttpClientErrorException e) {
            throw new SymbolNotFoundException();
        }
    }
}
