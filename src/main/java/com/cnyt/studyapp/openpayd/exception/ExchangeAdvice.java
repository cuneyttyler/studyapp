package com.cnyt.studyapp.openpayd.exception;

import com.cnyt.studyapp.openpayd.dto.ResponseObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExchangeAdvice {

    @ResponseBody
    @ExceptionHandler(TransactionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ResponseEntity<ResponseObject> transactionNotFoundHandler(TransactionNotFoundException ex) {
        return new ResponseEntity<>(new ResponseObject(ex.getMessage(),HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler(SymbolNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ResponseEntity<ResponseObject> symbolNotFoundHandler(SymbolNotFoundException ex) {
        return new ResponseEntity<>(new ResponseObject(ex.getMessage(),HttpStatus.BAD_REQUEST.value()),HttpStatus.BAD_REQUEST);
    }
}
