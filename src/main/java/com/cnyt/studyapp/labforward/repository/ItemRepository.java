package com.cnyt.studyapp.labforward.repository;

import com.cnyt.studyapp.labforward.model.ItemModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemModel, Long> {
}
