package com.cnyt.studyapp.labforward.service.impl;

import com.cnyt.studyapp.labforward.dto.CategoryDto;
import com.cnyt.studyapp.labforward.dto.ItemDto;
import com.cnyt.studyapp.labforward.exception.AttributeDoesNotBelongToCategoryException;
import com.cnyt.studyapp.labforward.exception.CategoryNotFoundException;
import com.cnyt.studyapp.labforward.exception.ItemNotFoundException;
import com.cnyt.studyapp.labforward.exception.MoreThanOneOfSameAttributeException;
import com.cnyt.studyapp.labforward.model.CategoryModel;
import com.cnyt.studyapp.labforward.model.ItemModel;
import com.cnyt.studyapp.labforward.repository.CategoryRepository;
import com.cnyt.studyapp.labforward.repository.ItemRepository;
import com.cnyt.studyapp.labforward.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ItemRepository itemRepository;

    public List<ItemDto> findItemsByCategory(Long categoryId) {
        CategoryModel categoryModel = categoryRepository.findById(categoryId).orElseThrow(() -> new CategoryNotFoundException(categoryId));
        return categoryModel.getItems().stream().map(im -> im.toDto(true,true)).collect(Collectors.toList());
    }

    public CategoryDto createCategory(CategoryDto categoryDto) {
        CategoryModel categoryModel = categoryDto.toModel(true,true);
        categoryModel.getAttributes().forEach(a -> a.setCategory(categoryModel));
        return categoryRepository.save(categoryModel).toDto(true,true);
    }

    public ItemDto createItem(ItemDto itemDto) {
        ItemModel itemModel = itemDto.toModel(true,true);
        checkAttributeIntegrity(itemModel);
        return itemRepository.save(itemModel).toDto(true,true);
    }

    public ItemDto updateItem(ItemDto itemDto) {
        ItemModel itemModel = itemDto.toModel(true,true);
        itemRepository.findById(itemModel.getId()).orElseThrow(() -> new ItemNotFoundException(itemModel.getId()));;
        checkAttributeIntegrity(itemModel);
        return itemRepository.save(itemModel).toDto(true,true);
    }

    private void checkAttributeIntegrity(ItemModel itemModel) {
        Map<Long,Long> map = itemModel.getAttributeValues().stream().collect(Collectors.groupingBy(av -> av.getAttribute().getId(), Collectors.counting()));
        map.values().forEach(c -> { if(c > 1) throw new MoreThanOneOfSameAttributeException(); });

        CategoryModel category = categoryRepository.findById(itemModel.getCategory().getId()).orElseThrow(() -> new CategoryNotFoundException(itemModel.getCategory().getId()));
        itemModel.getAttributeValues().forEach(av -> {
            Long count = category.getAttributes().stream().filter(a -> a.getId().equals(av.getAttribute().getId())).count();
            if(count == 0) {
                throw new AttributeDoesNotBelongToCategoryException(category.getId());
            } else {
                av.setItem(itemModel);
            }
        });
    }
}
